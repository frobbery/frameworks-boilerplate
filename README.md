
```
# Install requirements
pip3 install -r requirements.txt --user
# Create ablemic table in db (on first run only)
flask db init
# Create migration file
flask db migrate
# Apply migration to database
flask db upgrade
# Run tests 
python3 tests.py
# Run app
python3 -m flask run --host=0.0.0.0 --port=5006 # red06
Curl 85.192.35.28:5001/api/equipment
```

