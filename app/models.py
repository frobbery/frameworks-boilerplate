from enum import Enum
from app import db
from flask import url_for


class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page, **kwargs),
                'next': url_for(endpoint, page=page + 1, per_page=per_page, **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, per_page=per_page, **kwargs) if resources.has_prev else None
            }
        }
        return data


class JsonMixin(object):
    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def from_dict(self, data):
        for c in self.__table__.columns:
            if c.name in data:
                setattr(self, c.name, data[c.name])


class Equipment(db.Model, PaginatedAPIMixin, JsonMixin):
    # Серийный номер оборудования
    id = db.Column(db.Integer, primary_key=True)
    # Наименование оборудования (строка максимум 50 символов)
    equipment_model = db.Column(db.String(50))
    # ФИО, сдавшего оборудование (строка максимум 100 символов)
    equipment_fio = db.Column(db.String(100))
    # Телефонный номер,сдавшего оборудования
    equipment_phone_number = db.Column(db.Integer)






